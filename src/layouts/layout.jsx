import React from 'react'
import Header from '../components/Header/header.component'
import Footer from '../components/Footer/footer.component'
const Layout = props => {
    return (
        <div>
            <Header />
            <div className='main-container'>{props.children}</div>
            <Footer />
        </div>
    )
}

export default Layout