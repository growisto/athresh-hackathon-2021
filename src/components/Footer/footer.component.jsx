import React from 'react'

const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer-inner">
                <ul className='footer-links'>
                    <li>
                        <a>About Us</a>
                        <ul className='sub-links'>
                            <li><a>Our Promise</a></li>
                            <li><a>Blog</a></li>
                            <li><a>In News</a></li>
                            <li><a>Contact Us</a></li>
                        </ul>
                    </li>
                    <li>
                        <a>Help</a>
                        <ul className='sub-links'>
                            <li><a>FAQs</a></li>
                            <li><a>Laundry Care</a></li>
                            <li><a>Track Order</a></li>
                            <li><a>Shipping &amp; Returns</a></li>
                        </ul>
                    </li>
                    <li className='hide-xs'>
                        <a className='hide-lg'>Terms and Condition</a>
                        <ul className='sub-links'>
                            <li><a>Contact Us</a></li>
                            <li><a>Privacy</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div className="footer-text">
                © 2019, California Design Den®. All Rights Reserved.
            </div>
        </footer>
    )
}

export default Footer