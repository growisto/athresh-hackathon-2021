import React from 'react'
import './whycdd.styles.scss'
const WhyCddComponent = () => {
    return (
        <>
            <div className="row">
                <div className="col">
                    <div className="cdd">
                        <div className="icon"></div>
                        <h6 className="cdd-ques">
                            <span>Why</span>
                            <br></br>
                            California Design Den?
                        </h6>
                        <p className="cdd-ans">
                            Sometimes, you need to take one step back to go two
                            steps forward. With our products, we offer a
                            refreshed beginning after a comfortable pause. We
                            don’t just sell you products, we give you a place to
                            rewind and rejuvenate.
                            <br></br>
                            From the heart of our own factory, we deliver you
                            affordable and top quality sheets hand-designed by
                            our team of skilled artisans.
                        </p>
                        <a href="">
                            Read More <span>{'>'}</span>
                        </a>
                    </div>
                </div>
                {/* <video ref="vidRef" src="https://www.youtube.com/watch?v=xoh_L3KPpks&ab_channel=Codevolution" type="video/mp4"></video> */}
            </div>
        </>
    )
}

export default WhyCddComponent