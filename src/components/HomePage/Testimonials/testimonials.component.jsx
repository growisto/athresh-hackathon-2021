import React, { Component, useState } from 'react'
import Slider from 'react-slick'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import './testimonials.styles.scss'

const sliderData = [
    {
        id: 0,
        
        content:
            'Discover original, handmade designs crafted to give your home a personality.',
    },
    {
        id: 1,
        
        content:
            'Experience the warmth of the best cotton to be ever woven into sheets.',
    },
    {
        id: 2,
        
        content:
            'Taste the affordability of luxurious in-house manufactured products.',
    },
]

class Testimonials extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            arrows: false,
            autoPlay: true,
            dotsClass: 'slide-dots',
        }

        return (
            <>  <h3 className="slider-title">Expert Testimonials</h3>
                <Slider {...settings}>
                    {sliderData.map(slide => (
                        <div className="slider" key={slide.id}>
                            <div className="slider-tent">
                                <h3 className="slider-title">{slide.title}</h3>
                                <p className="slider-content">{slide.content}</p>
                                <button className="slider-button">Shop Now</button>
                            </div>
                            <div className="slider-img">
                                <img src='https://picsum.photos/seed/picsum/200/300'></img>
                            </div>
                        </div>
                        
                    ))}
                </Slider>
            </>
        )
    }
}

export default Testimonials