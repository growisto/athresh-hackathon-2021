import React from 'react'
import './more_products.styles.scss'
const MoreProducts = () => {
    return (
        <div className="more-products">
            <h4 className="title">More Products</h4>
                <div className="row">
                    <div className="col">
                        
                        <div className="category-box">
                            <img src="https://picsum.photos/seed/picsum/200/300"></img> 
                        </div>
                        <span className="title">Comforters</span>
                    </div>
                    <div className="col">
                        <div className="category-box">
                            <img src="https://picsum.photos/seed/picsum/200/300"></img> 

                        </div>
                        <span className="title">Duvet Covers</span>
                    </div>
                    <div className="col">
                        <div className="category-box">
                            <img src="https://picsum.photos/seed/picsum/200/300"></img> 
                        </div>
                        <span className="title">Quits</span>
                    </div>
                </div>
            </div>
        
    )
}

export default MoreProducts