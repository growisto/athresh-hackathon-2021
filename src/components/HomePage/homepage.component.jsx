import React, { Component } from 'react'
import WhyCddComponent from './cdd/whycdd.component'
import MoreProducts from './MoreProducts/more_products.component'
import Homepage_Slider from './Slider/slider.component'
import Testimonials from './Testimonials/testimonials.component'


class Homepage extends Component {
    render() {
        return (
            <>
                <div className="hide-lg">
                    <Homepage_Slider />
                    <WhyCddComponent />
                    <Testimonials />
                    <MoreProducts />
                    
                </div>
            </>
        )
    }
}

export default Homepage