import React, { useState } from 'react'
import './header.styles.scss'
const Header = () => {
    return (
        <header className="header">
            <div className="header-inner">
                

                <a className="logo"></a>
                <ul className="header-links">
                    <li>
                        <a href="#">Sheets</a>
                    </li>
                    <li>
                        <a href="#">Bedding</a>
                    </li>
                    <li>
                        <a href="#">Our Promise</a>
                    </li>
                    <li>
                        <a href="#">In Media</a>
                    </li>
                    <li>
                        <a href="#">My Account</a>
                    </li>
                    <li>
                        <a href="#">Free shipping in united states</a>
                    </li>
                </ul>
                
            </div>
        </header>)}

export default Header